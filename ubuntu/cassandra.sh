cd ~
wget http://apache.spd.co.il/cassandra/2.0.5/apache-cassandra-2.0.5-bin.tar.gz
tar -xvzf apache-cassandra-2.0.5-bin.tar.gz
rm apache-cassandra-2.0.5-bin.tar.gz
cd apache-cassandra-2.0.5/bin/
sudo mkdir /var/lib/cassandra
sudo chown vagrant:vagrant /var/lib/cassandra/
sudo mkdir /var/log/cassandra
sudo chown vagrant:vagrant /var/log/cassandra/
nohup ./cassandra &

